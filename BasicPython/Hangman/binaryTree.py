"""
BINARY TREES:
=============

A binary tree is a tree in which each node brings a label and has at most two children,
a left child and a right one. 
You will see in the "Algorithms and Complexity" course that 
binary trees are widely used in computer science (e.g. sorting, heaps).
"""

"""
Using a dictionary to implement a binary tree:
----------------------------------------------

We can use dictionaries to implement a binary tree.
We create a dictionary for each node/subtree starting from the leaves until reaching the root.
"""

# For instance the following code builds the following tree:
#       r
#     /   \
#    /     \
#   A       B
#    \     /
#     C   D
bt1_node_C = {'label':  'C'}                            # Leaf with label 'C'
bt1_node_D = {'label':  'D'}                            # Leaf with label 'D'
bt1_node_A = {'label':  'A', 'right':bt1_node_C}         # The subtree starting at A
bt1_node_B = {'label':  'B', 'left':bt1_node_D}        # The subtree starting at B
bt1_root   = {'label': 'r', 'left':bt1_node_A, 'right':bt1_node_B} 

# We will use this tree under the name 'bt1' in the next questions. 
bt1 = bt1_root                                 # our first binary tree

# Alternatively, we can first create separate nodes and connect them after: 
bt2_nodes = [{'label':i} for i in range(11)] # create a list of 11 nodes labelled 0 to 10
for i in range(5):
    bt2_nodes[i]['left'] = bt2_nodes[2*i+1]
    bt2_nodes[i]['right'] = bt2_nodes[2*(i+1)]
 
bt2 = bt2_nodes[0]
# It creates the following tree:
#           0
#         /   \
#       /       \
#      1         2
#    /   \      / \
#   3     4    5   6
#  / \   / \
# 7   8 9  10

"""
Question 1

In a binary tree, each node/subtree (except the root node) has a unique parent. 
We want to be able to access that parent.
Update the dictionaries of bt1 and bt2 trees by adding a parent entry to each node.
"""
# INSERT YOUR CODE HERE for bt1 AND UNCOMMENT THE FOLLOWING TESTS

#assert 'parent' not in bt1_root                             # bt1_root is the root
#assert bt1['right']['parent'] == bt1                        # r is the parent of B 
#assert bt1['left']['parent']  == bt1                        # r is the parent of A
#assert bt1['right']['left']['parent'] == bt1['right']       # B is the parent of D
#assert bt1['left']['right']['parent']  == bt1['left']       # A is the parent of C

# INSERT YOUR CODE HERE for bt2 AND UNCOMMENT THE FOLLOWING TESTS

#assert ('parent' in bt2) == False                                       # 0 is the root
#assert bt2['left']['parent'] == bt2                                     # 0 is parent of 1
#assert bt2['right']['parent'] == bt2                                    # 0 is parent of 2
#assert bt2['left']['left']['parent'] == bt2['left']                     # 1 is parent of 3
#assert bt2['left']['right']['parent'] == bt2['left']                    # 1 is parent of 4
#assert bt2['right']['left']['parent'] == bt2['right']                   # 2 is parent of 5
#assert bt2['right']['right']['parent'] == bt2['right']                  # 2 is parent of 6
#assert bt2['left']['left']['left']['parent'] == bt2['left']['left']     # 3 is parent of 7
#assert bt2['left']['left']['right']['parent'] == bt2['left']['left']    # 3 is parent of 8
#assert bt2['left']['right']['left']['parent'] == bt2['left']['right']   # 4 is parent of 9
#assert bt2['left']['right']['right']['parent'] == bt2['left']['right']  # 4 is parent of 10

"""
Question 2

We call the depth of a node (subtree), its distance to the root.
For example, in bt2 the depth is 1 for nodes 1 and 2 and it is 2 for node 3.

Write an iterative function depth_iter that takes one node (the corresponding Python dictionary) 
as parameter and returns its depth. 
"""
def depth_iter(bt):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#assert depth_iter(bt1) == 0
#assert depth_iter(bt1_node_A) == 1
#assert depth_iter(bt1_node_B) == 1
#assert depth_iter(bt1_node_C) == 2
#assert depth_iter(bt1_node_D) == 2

#assert depth_iter(bt2_nodes[10]) == 3

"""
Question 3

We want to compute the path from the root to a given node (subtree).
For example, in bt2 the path to node 5 is the sequence ['right', 'left'] 
and to node 10 is ['left', 'right', 'right'].

Write an iterative function path_iter that takes a node as parameter and returns its path.
"""
def path_iter(bt):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#assert path_iter(bt1) == []
#assert path_iter(bt1_node_A) == ['left']
#assert path_iter(bt1_node_B) == ['right']
#assert path_iter(bt1_node_C) == ['left', 'right']
#assert path_iter(bt1_node_D) == ['right', 'left']

#assert path_iter(bt2_nodes[5]) == ['right', 'left']
#assert path_iter(bt2_nodes[10]) == ['left', 'right', 'right']

"""
RECURSION

Many problems are recursive, 
which means that the solution depends on solutions to smaller instances of the same problem. 
In computer science, recursive functions solve such recursive problems 
by calling themselves from within their own code.

A recursive function has a typical form:
* stopping condition : the base case that is mandatory to avoid an infinite calls loop.
* recursion :
  - pre-recursion : code executed before the recursive calls.
  - recursion calls
  - post-recursion : code executed after the recursive calls. 

Factorial is an illustrative example:
"""
def FACT_rec(n):
    ## BASE CASE : stopping condition
    if n == 1:
        return 1
    ## RECURSION
    # pre-recursion
    n_1 = n-1
    # recursive calls
    fact_n_1 = FACT_rec(n_1)
    # post-recursion
    res = n * fact_n_1
    return res

"""
Question 4

Write the recursive versions depth_rec and path_rec 
of the previous iterative functions depth_iter and path_iter.
"""
def depth_rec(bt):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#for node in bt2_nodes:
#    assert depth_rec(node) == depth_iter(node)

def path_rec(bt):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#assert path_iter(bt1) == path_rec(bt1)
#assert path_iter(bt1_node_A) == path_rec(bt1_node_A)
#assert path_iter(bt1_node_B) == path_rec(bt1_node_B)
#assert path_iter(bt1_node_C) == path_rec(bt1_node_C)
#assert path_iter(bt1_node_D) == path_rec(bt1_node_D)

#for node in bt2_nodes:
#    assert path_rec(node) == path_iter(node)
#    assert path_rec(node) == path_iter(node)

"""
Question 5

The 2 previous recursive functions have a single recursive call within their code.
Write a recursive function height(bt) to compute the height of a given binary tree.
How many recursive calls do we need? 
"""
def height(bt):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#assert height(bt1_node_C) == 0
#assert height(bt1_node_A) == 1
#assert height(bt1) == 2
#assert height(bt2) == 3

"""
Question 6 (advanced only)

Before playing with some binary tree implemented in our data structure,
we need to check first whether it is well formed.
It consists in 2 conditions:
* Each node of the tree holds a label;
* If node n is a left or right child of node p then p is the parent of n. 

Write a recursive function well_formed(bt) checking that the tree bt is well formed 
and returning True or False.
"""
def well_formed(bt):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError
        
#assert well_formed(bt1)
#assert well_formed(bt2)
 
## deep copies before alteration
#from copy import deepcopy 
#bt1_test1 = deepcopy(bt1)
#bt1_test2 = deepcopy(bt1)  
#bt1_test3 = deepcopy(bt1)
 
## no label
#bt1_test1['right']['left'].pop('label')
#assert not well_formed(bt1_test1)
 
## child without parent
#bt1_test2['right']['left'].pop('parent')
#assert not well_formed(bt1_test2)
 
## wrong parent
#bt1_test3['right']['left']['parent'] = bt1_test1['left']
#assert not well_formed(bt1_test3)

"""
BINARY TREE TRAVERSAL

One pattern for traversing a binary tree is to recursively traverse its subtrees.
We distinguish 3 variants:
 * PRE-order: when the current node is visited before its subtrees.
 * IN-order: when the current node is visited in-between the visits of the two subtrees.
 * POST-order: when the current node is visited after the subtrees. 
In the 3 cases, we visit the left subtree before the right one.
Visiting a node consists in a treatment of the data it encapsulates (e.g. printing its label). 
"""

"""
Question 7

Write a funtion in_order(bt) that visits the tree bt and 
returns the sequence of the labels of bt following the IN-order.
"""
def in_order(bt):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError
 
#assert in_order(bt1) == ['A', 'C', 'r', 'D', 'B']
#assert in_order(bt2) == [7, 3, 8, 1, 9, 4, 10, 0, 5, 2, 6]

"""
Question 8

The mirror of a binary tree is a new binary tree with left and right children 
of all nodes interchanged. 
Write a recursive function mirror(bt) which computes and returns the mirror of a given tree bt.
"""
def mirror(bt):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

## Advanced students who wrote the function well_formed(bt) should verify first: 
#assert well_formed(mirror(bt1))
#assert well_formed(mirror(bt2))

## To test your code, you may note that mirroring implies reversing the visiting IN-order:
#assert in_order(mirror(bt1)) == list(reversed(in_order(bt1)))
#assert in_order(mirror(bt2)) == list(reversed(in_order(bt2)))

"""
Question 9 (advanced only)

Write a recursive function equiv(bt1,bt2) to check whether two well-formed binary trees are similar
and return True or False.
bt1 and bt2 are similar when they have the same structure holding the same labels.
We suppose that well-formedness of bt1 and bt2 was verified before calling equiv. 
"""
def equiv(bt1, bt2):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#bt1_test = deepcopy(bt1)
#assert equiv(bt1, bt1_test)

## same labels, change the position of node D:
#bt3_node_C = {'label':  'C'} 
#bt3_node_D = {'label':  'D'} 
#bt3_node_A = {'label':  'A', 'right':bt3_node_C}
#bt3_node_B = {'label':  'B', 'right':bt3_node_D}
#bt3_root   = {'label': 'r', 'left':bt3_node_A, 'right':bt3_node_B}  
#assert not equiv(bt1, bt3_root) 

## a different node label:
#bt1_test['right']['label'] = 'Z'
#assert not equiv(bt1, bt1_test)
 
## different structures
#assert not equiv(bt1, bt2)

"""
Explain what happens when you run the following: 
(and comment it out again before working on the next question)
"""
#bt1_test = deepcopy(bt1)
#assert bt1 == bt1_test 

"""
Question 10 (advanced only)

Suppose each node has a unique Id as a label. 
If we have both IN-order and PRE-order visiting sequences, we can build the corresponding binary tree. 
Write a recursive function rebuild(in_seq, pre_seq) to do so.
"""
def rebuild(in_seq, pre_seq):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#assert equiv(bt1, rebuild(['A','C','r','D','B'],['r','A','C','B','D']))
#assert equiv(bt2, rebuild([7, 3, 8, 1, 9, 4, 10, 0, 5, 2, 6], [0, 1, 3, 7, 8, 4, 9, 10, 2, 5, 6]))
 

