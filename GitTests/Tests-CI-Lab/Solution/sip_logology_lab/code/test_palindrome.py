import pytest
import palindrome

def test_isPalindrome():
    assert palindrome.isPalindrome("dog") == False, "dog should not be considered a palindrome"
    assert palindrome.isPalindrome("kayak") == True, "kayak should be considered a valid palindrome"

def test_isPalindrome_with_space():
    assert palindrome.isPalindrome("never odd or even") == True, "space character should be ignored"

def test_isPalindrome_with_mixed_case():
    assert palindrome.isPalindrome("Bob") == True, "palindrome are not case sensitive"

def test_isPalindrome_with_punctuation():
    assert palindrome.isPalindrome("Was it a car or a cat I saw?") == True, "ponctuation characters should be ignored"
    assert palindrome.isPalindrome("a man, a plan, a canal: panama!") == True, "ponctuation characters should be ignored" 
