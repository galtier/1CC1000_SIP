{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to use NumPy\n",
    "\n",
    "The objective of these examples is to see how an effective use of NumPy makes it possible to carry out efficient calculations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## When is NumPy effective?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### List of lists\n",
    "\n",
    "The following code:\n",
    "- creates a list of lists, `A`, randomly generated, containing a large number of 0 and 1;\n",
    "- makes a second list of lists, the same size as `A` and which we subtly call `B`;\n",
    "- copies the values from `A` to `B`. \n",
    "- measures the execution time of the *copy* opeation.\n",
    "\n",
    "You should have a time of the order of a few hundredths of seconds. Test it. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from random import randint\n",
    "from timeit import timeit\n",
    "\n",
    "A = [[randint(0,1) for _ in range(300)] for _ in range(600)]\n",
    "B = [[0 for _ in range(300)] for _ in range(600)]\n",
    "\n",
    "def copy(A, B):\n",
    "    for i in range(0, len(A)):\n",
    "        for j in range(0, len(A[0])):\n",
    "            B[i][j] = A[i][j]\n",
    "\n",
    "timeit(\"copy(A, B)\", number=10, globals=globals())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### NumPy array\n",
    "\n",
    "The following code is used to perform an equivalent operation using NumPy.\n",
    "\n",
    "Compare its run time. Does this surprise you? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "A = np.random.randint(0, 2, (300, 600))\n",
    "B = np.zeros(A.shape)\n",
    "\n",
    "def copy(A, B):\n",
    "    for i in range(0, len(A)):\n",
    "        for j in range(0, len(A[0])):\n",
    "            B[i,j] = A[i,j]\n",
    "\n",
    "timeit(\"copy(A, B)\", number=10, globals=globals())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The \"good way\"\n",
    "\n",
    "It seems that using NumPy is less effective than using lists. The reality is that we are not using NumPy functionality properly. Indeed, this library is optimized for \"vectorization\" or \"array programming\". It is a technique of applying operations to one set of data at a time.\n",
    "\n",
    "To take advantage of NumPy, you must therefore change your approach and use the features offered by this library. For example, the correct way to perform a copy operation is the following. You should agree that it is significantly more effective... "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.random.randint(0, 2, (300, 600))\n",
    "B = np.zeros(A.shape)\n",
    "\n",
    "def copy(A, B):\n",
    "    A[:,:] = B[:,:]\n",
    "\n",
    "timeit(\"copy(A, B)\", number=10, globals=globals())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The `vectorize` operation\n",
    "\n",
    "In NumPy, every mathematical operation with arrays is automatically vectorized. If you wrote your own function and want to see it applied to every elements of an array without using loops, use the `vectorize()` operation :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_function(x):\n",
    "    if x < 0:\n",
    "        return 0\n",
    "    else:\n",
    "        if x > 1000000:\n",
    "            return x - 1000000\n",
    "        else:\n",
    "            return x*x\n",
    "\n",
    "my_vectorized_function = np.vectorize(my_function)\n",
    "\n",
    "def v1(A):\n",
    "    for i in range(0, len(A)):\n",
    "        for j in range(0, len(A[0])):\n",
    "            A[i,j] = my_function(A[i,j])\n",
    "\n",
    "Da = np.random.randint(2, 5, (2, 3))\n",
    "Db = np.zeros(Da.shape, dtype=int)\n",
    "Db[:,:] = Da[:,:]\n",
    "print(\"Da:\", Da)\n",
    "print(\"Db:\", Db)\n",
    "\n",
    "print(\"check the results:\") \n",
    "print(\"without vectorization:\")\n",
    "v1(Da)\n",
    "print(Da)\n",
    "\n",
    "print(\"with vectorization:\")\n",
    "Db = my_vectorized_function(Db)\n",
    "print(Db)\n",
    "\n",
    "Da = np.random.randint(-2, 3, (1000, 1000))\n",
    "Db = np.zeros(Da.shape, dtype=int)\n",
    "Db[:,:] = Da[:,:]\n",
    "print(\"without vectorization: \", timeit(\"v1(Da)\", number=10, globals=globals()))\n",
    "print(\"with vectorization:    \", timeit(\"my_vectorized_function(Db)\", number=10, globals=globals()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to use NumPy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Slice of lists\n",
    "\n",
    "You already know the slice of lists. Here is an example, we note that :\n",
    "- when used to the left of an equal sign, it is used to set values in a list;\n",
    "- when used to the right of an equal sign, it is used to duplicate part of a list. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "l = [0, 1, 2, 3, 4, 5]\n",
    "print(\"Id of l                     : \", id(l))\n",
    "print(\"initial content of l        :  \", l)\n",
    "\n",
    "# slice on the left of =  -> set values\n",
    "print(\"change the 3 first values stored in l\")\n",
    "l[:3] = [2, 1, 0]\n",
    "print(\"Id of l is unchanged        : \", id(l))\n",
    "print(\"new content of l            :  \", l)\n",
    "\n",
    "# slice on the rigth of = -> duplicate part of a list\n",
    "print(\"l2 is a copy of a sub-part of l\")\n",
    "l2 = l[:4]\n",
    "print(\"Id of l2 differs from id of l: \", id(l2))\n",
    "print(\"content of l2                : \", l2)\n",
    "\n",
    "print(\"change the values stored in l2\")\n",
    "l2[:] = [0, 0, 0, 0] # Warning do not confuse with \"l2 = [0, 0, 0, 0]\" \n",
    "                     # which makes l2 points towards a new reference\n",
    "print(\"new content of l2           :  \", l2)\n",
    "print(\"content of l is unchanged   :  \", l)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Views\n",
    "\n",
    "Using the bracket notation on a NumPy array does not create a slice, it works differently from the lists. The bracket notation to the left or to the right of an equal sign will always create a view, which is an access to the data but is never a copy of the data.\n",
    "\n",
    "Test the following code. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.array([0, 1, 2, 3, 4, 5])\n",
    "print(\"Id of A                     : \", id(A))\n",
    "print(\"initial content of A        :  \", A)\n",
    "\n",
    "# bracket notation on the left of =\n",
    "print(\"change the 3 first values stored in A\")\n",
    "A[:3] = [2, 1, 0]\n",
    "print(\"Id of A is unchanged        : \", id(A))\n",
    "print(\"new content of A            :  \", A)\n",
    "\n",
    "# bracket notation of the right of =\n",
    "print(\"A2 is a view on part of A\")\n",
    "A2 = A[:4]\n",
    "print(\"Id of A2 differs from id of A: \", id(A2))\n",
    "print(\"content of A2                : \", A2)\n",
    "\n",
    "print(\"change the values stored in A2\")\n",
    "A2[:] = [0, 0, 0, 0]\n",
    "print(\"new content of A2                 :  \", A2)\n",
    "print(\"content of A is changed as well!  :  \", A)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Array or view can most of the time be used without distinction. When handling a view, the base attribute allows us to find the corresponding array. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Base of A  :  \", A.base)\n",
    "print(\"Base of A2 : \", A2.base)\n",
    "\n",
    "print(\"Id of A              : \", id(A))\n",
    "print(\"Id of the base of A2 : \", id(A2.base))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A view is a link to data and an organization of data. With reshape we can create new views towards data which can be organized differently. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#A = np.array([0, 1, 2, 3, 4, 5])\n",
    "V3 = np.reshape(A, (2, 3))\n",
    "print(\"V3 from reshape of A  :\\n\", V3)\n",
    "V3[1, 0] = 9\n",
    "print(\"V3 after value update :\\n\", V3)\n",
    "print(\"A after V3 is modified:\\n\", A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Indexing\n",
    "\n",
    "It is possible to reference indices in ways quite similar to the slice (start, end, step), however when handling NumPy arrays with several dimensions, it is not necessary to chain the bracket notations (to access lists of lists of ...), just use commas.\n",
    "\n",
    "Finally, we can fix the values contained in a view using another view of the same size and same dimension or a scalar.\n",
    "\n",
    "For more informations : [indexing ](https://numpy.org/doc/stable/reference/arrays.indexing.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.ones((3, 4))\n",
    "print(\"Array A :\\n\", A)\n",
    "\n",
    "A[::2, :] = 2 # lignes 0 et 2, toutes les colonnes, remplir de 2\n",
    "print(\"Array A : modifié avec un scalaire\\n\", A)\n",
    "\n",
    "B = np.zeros((3, 4))\n",
    "A[:,:] = B[:,:]\n",
    "print(\"Array A : modifié avec une vue\\n\", A)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.4 64-bit",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
