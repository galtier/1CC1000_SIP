# Example of various widgets
#===========================
# This file showcases some of Tkinter widgets.

# Except for labels, that just display text or image, widgets capture input from the user.
# In this example we ignore these inputs.

import tkinter as tk

window = tk.Tk()

# a label that displays a text
tk.Label(window, text="Hello World!").pack()

# a label with a specified size
# Note that, althought width and height have the same value, the label isn't square. 
# This is because the size is specified in text units, that is the width (respectively, the height) 
# of the character 0 (the number zero).
# This choice guarantees that the appearance of the widget is consistent across platforms.
# The widget will always have the size to display 10 zeroes, both horizontally and vertically, 
# independently of the font used in the system where the application is run. 
tk.Label(window, text="Hello larger World!", width=20, height=20).pack()

# a label that displays an image
from PIL import Image, ImageTk
image = ImageTk.PhotoImage(Image.open("ducky.jpeg"))
tk.Label(window, image=image).pack()

# a button that does nothing when pressed
tk.Button(window, text="I'm useless...").pack()
# a yellow button 
tk.Button(window, text="I'm useless but I look good!", background='yellow').pack()

# a field where the user can enter text (limited to one line; for longer input use the 'Text' widget)
tk.Entry(window).pack()
# an entry field with pre-entered text
entry = tk.Entry(window)
entry.insert(0, "you can type text here!")
entry.pack()

window.mainloop()



"""
window = tk.Tk()

# a scale bar from 5 to 10 (!!! from_  with a trailing underscore)
tk.Scale(window, orient='horizontal', from_=5, to=10).pack()

# a check button
tk.Checkbutton(window, text="done").pack()

# two radio buttons in the same group (selecting one automatically unselect the other one if it was selected)
# All radio buttons in a group are associated with the same variable, 
# pushing a button changes the value of this variable to the specified value.
v=tk.IntVar()
tk.Radiobutton(window, text="first choice", variable=v, value=1).pack()
tk.Radiobutton(window, text="second choice", variable=v, value=2).pack()

# an option menu
value = tk.StringVar(value='pick a name')
tk.OptionMenu(window, value, 'Alice', 'Bob', 'Charly').pack()

# a list box
fruitsListBox = tk.Listbox(window)
fruitsListBox.insert(tk.END,"apple")
fruitsListBox.insert(tk.END, "banana")
fruitsListBox.insert(tk.END, "cherry")
fruitsListBox.pack()

# another entry field, larger than \"Entry\", with some pre-set text
novelText = tk.Text(window)
novelText.insert(tk.END, "write a short novel")
novelText.pack()


window.mainloop()
"""
