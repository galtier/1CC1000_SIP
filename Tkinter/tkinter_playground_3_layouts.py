import tkinter as tk

# Layouts
#========
# This file showcases how to arrange widgets within a window.
# The ```pack``` geometry manager
#--------------------------------
#That's the geometry manager we've been using so far. It's called with ```pack()```.
# With *pack* the default placement is made top towards bottom, horizontically centered.
window = tk.Tk()

tk.Label(window, text="Alice", background='pink').pack()
tk.Label(window, text="Bob", background='blue').pack()
tk.Label(window, text="Charly", background='green').pack()

window.mainloop()
# ```side``` argument of the ```pack``` function
#-----------------------------------------------
# The argument ```side``` of the function ```pack``` force the geometry manager to add the widgets against the left, right or bottom side of the window, instead of the top one.
"""
window = tk.Tk()

tk.Label(window, text="Alice", background='pink').pack()               # Alice is horizontally centered in the topmost part of the window
tk.Label(window, text="Bob", background='blue').pack(side=tk.LEFT)     # Bob is in the left-hand side of the window space below Alice, vertically centered
tk.Label(window, text="Charly", background='green').pack()              # Charly is horizontally centered in the topmost part of the window space below Alice and to the right of Bob
tk.Label(window, text="Emma", background='yellow').pack(side=tk.RIGHT)   # Emma is in the right-hand side of the window space below Charly, vertically centered
# to better observe the placement, enlarge/resize the window
window.mainloop()
"""
# the **grid** geometry manager
#------------------------------
# The *pack* geometry manager is quite powerful, but it's difficult to use. An equally powerful, yet more intuitive geometry manager, is the *grid* geometry manager. It allows the placement of widgets into a grid with a given number of rows and columns (indexes start at 0)
"""
window = tk.Tk()

tk.Label(window, text="Alice", background='pink').grid(row=0, column=0)
tk.Label(window, text="Bob", background='blue').grid(row=0, column=1)
tk.Label(window, text="Charly", background='green').grid(row=1, column=0)

window.mainloop()
"""
# padding
#--------
# In the window created by the previous snippet, there is no space between the labels, which makes the GUI a little cluttered. We can free some space around the labels by using the arguments ```padx``` and ```pady``` of the ```grid``` function
"""
window = tk.Tk()

tk.Label(window, text="Alice", background='pink').grid(row=0, column=0, padx=10, pady=10)
tk.Label(window, text="Bob", background='blue').grid(row=0, column=1, padx=10, pady=10)
tk.Label(window, text="Charly", background='green').grid(row=1, column=0)

window.mainloop()
"""
# Frames
#-------
# In the previous examples, we added the widgets directly into the window. However, for more sophisticated layouts it is useful to compose with *frames*. Each frame can hold a subset of the widgets. Frames are independent of one another, each frame can use a different geometry manager; a frame can use the *pack* geometry manager, while another (in the same window) can use the *grid* geometry manager; or, two frames can both use the *grid* geometry manager, while using a different number of rows and columns.
"""
window = tk.Tk()

boyFrame = tk.Frame(window)  # the parent is the window
girlFrame = tk.Frame(window, background='red') 

tk.Label(boyFrame, text="Bob", background='blue').pack() # the parent is THE FRAME
tk.Label(girlFrame, text="Alice", background='pink').grid(row=0, column=0, padx=10, pady=10)
tk.Label(girlFrame, text="Charly", background='green').grid(row=0, column=1, padx=10, pady=10)
tk.Label(girlFrame, text="Emma", background='yellow').grid(row=0, column=2, padx=10, pady=10)

boyFrame.pack()
girlFrame.pack()

window.mainloop()
"""
# ```fill``` and ```expand``` when packing a frame
#-------------------------------------------------
# You certainly noticed that the frame don't expand while resizing the window (the red area stays the same). In order to enable this behaviour, we can use the arguments ```fill``` and ```expand```. The possible values of ```fill``` are ```tk.X``` (the frame will fill the parent window horizontally), ```tx.Y``` (fill the parent window vertically) or ```tk.BOTH```; the possible values of ```expand``` are ```True``` (the frame will expand while the parent window is resized) or ```False``` (the frame does not follow the resize of the parent window).
"""
window = tk.Tk()

boyFrame = tk.Frame(window, background='white')
girlFrame = tk.Frame(window, background='red')

tk.Label(boyFrame, text="Bob", background='blue').pack() # the parent is THE FRAME
tk.Label(girlFrame, text="Alice", background='pink').grid(row=0, column=0, padx=10, pady=10)
tk.Label(girlFrame, text="Charly", background='green').grid(row=0, column=1, padx=10, pady=10)
tk.Label(girlFrame, text="Emma", background='yellow').grid(row=0, column=2, padx=10, pady=10)

boyFrame.pack(fill=tk.X, expand=False)
girlFrame.pack(fill=tk.BOTH, expand=True)

window.mainloop()
"""