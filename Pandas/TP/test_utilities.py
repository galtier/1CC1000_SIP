import utilities
import pytest
import pandas as pd

def test_get_date_as_ddmmyyyy():
    assert utilities.get_date_as_ddmmyyyy("12/31/2000") == "31/12/2000", "12/31/2000 should be 31/12/2000"

def test_get_date_as_ddmmyyyy_with_empty_value():    
    assert utilities.get_date_as_ddmmyyyy("") == None, "empty string should return None"
    
def test_get_date_as_ddmmyyyy_with_invalid_date():
    assert utilities.get_date_as_ddmmyyyy("11/31/2000") == None, "11/31/2000 is not a valid date"

def test_get_date_as_ddmmyyyy_with_invalid_input_format():
    assert utilities.get_date_as_ddmmyyyy("12/31/00") == None, "12/31/00 is not a valid date in mm/dd/yyyy format"
    assert utilities.get_date_as_ddmmyyyy("31/12/2000") == None, "31/12/2000 is not a valid date in mm/dd/yyyy format"
    
