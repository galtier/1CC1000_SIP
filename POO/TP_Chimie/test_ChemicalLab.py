import pytest

from ChemicalLab import CheminalReaction
from CommonMolecules import Methane, Dioxygen, CarbonDioxide, Water, Oxygen, Hydrogen, Carbon


def test_reaction():
    e1 = Methane()
    e2 = Dioxygen()
    e3 = Dioxygen()
    e4 = Dioxygen()
    reactives = [e1, e2, e3, e4]
    
    reactiveAtomIdList = []
    for e in reactives:
        for a in e.getAtoms():
            reactiveAtomIdList.append(id(a))

    result = CheminalReaction(reactives)
    
    residualAtomIdList = []
    for e in reactives:
        for a in e.getAtoms():
            residualAtomIdList.append(id(a))

    resultAtomIdList = []
    for e in result:
        for a in e.getAtoms():
            resultAtomIdList.append(id(a))

    resultAndResidualAtomIdList = resultAtomIdList + residualAtomIdList
    
    assert set(reactiveAtomIdList) == set(resultAndResidualAtomIdList)
