from abc import abstractclassmethod, abstractmethod
from ChemicalComponent import ChemicalComponent

class ChemicalElement(ChemicalComponent):

    # Le constructeur
    def __init__(self, name):
        # On invoque le constructeur de la classe mère
        # qui range le paramètre "name" dans l'attribut privé "__name"
        super().__init__(name)

    # Le nombre de protons caractérise un élément chimique :
    # 1 proton = hydrognène, 8 protons = oxygène etc
    # Le numéro atomique pourrait donc être un attibut de classe
    # (toutes les instances des atomes d'hydrogène ont 1 proton, etc).
    # Si on définit un attribut de classe ici, on ne pourra pas lui donner
    # de valeur, puisque cette valeur dépend du type d'élement chimique considéré.
    # Mais Python ne permet pas que des attributs de classe soient redéfinis dans
    # les classes filles.
    # On pourrait attendre d'être assez bas dans l'arbre d'héritage pour définir
    # l'attribut de classe au moment où on est capable de lui donner une valeur
    # (donc quand on écrit la classe Hydrogen par exemple). Mais l'inconvénient de
    # cette approche est qu'on pourrait créer des classes filles et oublier d'y
    # définir cet attribut. Un client itérant sur une liste de composants chimiques
    # et confiant qu'il peut demander à chacun son numéro atomique aura une mauvaise
    # surprise si la liste contient une instance de la classe où il y a l'oubli.
    # La solution retenue est donc de créer une méthode de classe, abstraite pour 
    # le moment mais qui peut (et devra) être redéfinie dans les classes filles :
    @classmethod
    @abstractmethod
    def get_atomic_number(cls):
        pass
    
    # même principe
    # notez qu'on peut cumuler les décorateurs comme pour get_atomic_number
    # mais qu'il existe également un décorateur qui combine les deux :
    @abstractclassmethod
    def get_symbol(cls):
        pass

# Impossible d'instancier la classe ChemicalElement car on n'a toujours pas redéfini
# la méthode abstraite get_mass héritée de ChemicalComponent
##ce = ChemicalElement("EssaiCE")
# TypeError: Can't instantiate abstract class ChemicalElement with abstract method get_mass