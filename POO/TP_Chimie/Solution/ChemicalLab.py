from CommonMolecules import Methane, Dioxygen, CarbonDioxide, Water, Oxygen, Hydrogen, Carbon



def CheminalReaction(molecules):

    def use_atom(atoms, kind):
        for a in atoms:
            if isinstance(a, kind):
                atoms.remove(a)
                return a
        return None


    methane_count = 0
    dioxygen_count = 0
    for m in molecules:
        if isinstance(m, Dioxygen):
            dioxygen_count += 1
        elif isinstance(m, Methane):
            methane_count += 1
    if methane_count >= 1 and dioxygen_count >= 2:
        methane_count = 0
        dioxygen_count = 0
        atoms = []
        for m in molecules:
            if isinstance(m, Dioxygen) and dioxygen_count < 2:
                atoms = atoms + (m.destroy())
                dioxygen_count += 1
            elif isinstance(m, Methane) and methane_count < 1:
                atoms = atoms + (m.destroy())
                methane_count += 1
        w1 = Water("w1", [use_atom(atoms, Hydrogen), use_atom(atoms, Hydrogen), use_atom(atoms, Oxygen)])
        w2 = Water("w2", [use_atom(atoms, Hydrogen), use_atom(atoms, Hydrogen), use_atom(atoms, Oxygen)])
        c = CarbonDioxide("c", [use_atom(atoms, Carbon), use_atom(atoms, Oxygen), use_atom(atoms, Oxygen)])
        return [w1, w2, c]
    else:
        return None

"""
e1 = Methane()
e2 = Dioxygen()
e3 = Dioxygen()
e4 = Dioxygen()
reactives = [e1, e2, e3, e4]
result = CheminalReaction(reactives)
"""