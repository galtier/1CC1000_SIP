from Atom import Atom

class Carbon(Atom):
    
    def __init__(self, name="Carbon", neutron_number=6):
        if not 6 <= neutron_number <= 8:
            raise TypeError("Invalid neutron number in Carbon")
        super().__init__(name, neutron_number)

    @classmethod
    def get_atomic_number(cls):
        return 6

    @classmethod
    def get_symbol(cls):
        return 'C'