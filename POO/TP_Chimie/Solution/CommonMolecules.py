from Modecule import Molecule
from Carbon import Carbon
from Hydrogen import Hydrogen
from Oxygen import Oxygen

class Methane(Molecule):
    def __init__(self, name = "Methane", atoms = [Carbon(), Hydrogen(), Hydrogen(), Hydrogen(), Hydrogen()]):
        super().__init__(name, atoms)
        d = self.get_composition()
        if len(d) != 2:
            raise TypeError("Bad atoms in methane")
        if not Hydrogen in d.keys() or d[Hydrogen] != 4:
            raise TypeError("Methane needs 4 atoms of Hydrogen")
        if not Carbon in d.keys() or d[Carbon] != 1:
            raise TypeError("Methane needs 1 atom of Carbon")

class Dioxygen(Molecule):
    def __init__(self, name = "Dioxygen", atoms = [Oxygen(), Oxygen()]):
        super().__init__(name, atoms)
        d = self.get_composition()
        if len(d) != 1:
            raise TypeError("Bad atoms in dioxigen")
        if not Oxygen in d.keys() or d[Oxygen] != 2:
            raise TypeError("Dioxygen needs 2 atoms of Oxygen")

class CarbonDioxide(Molecule):
    def __init__(self, name = "CarbonDioxide", atoms = [Carbon(), Oxygen(), Oxygen()]):
        super().__init__(name, atoms)
        d = self.get_composition()
        if len(d) != 2:
            raise TypeError("Bad atoms in carbon dioxide")
        if not Oxygen in d.keys() or d[Oxygen] != 2:
            raise TypeError("Carbon dioxide needs 2 atoms of Oxygen")
        if not Carbon in d.keys() or d[Carbon] != 1:
            raise TypeError("Carbon dioxide needs 1 atom of Carbon")

class Water(Molecule):
    def __init__(self, name = "Water", atoms = [Hydrogen(), Hydrogen(), Oxygen()]):
        super().__init__(name, atoms)
        d = self.get_composition()
        if len(d) != 2:
            raise TypeError("Bad atoms in water")
        if not Hydrogen in d.keys() or d[Hydrogen] != 2:
            raise TypeError("Water needs 2 atoms of Hydrogen")
        if not Oxygen in d.keys() or d[Oxygen] != 1:
            raise TypeError("Water needs 1 atom of Oxygen")


