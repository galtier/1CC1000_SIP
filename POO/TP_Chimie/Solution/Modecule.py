from ChemicalComponent import ChemicalComponent
from Atom import Atom
from Hydrogen import Hydrogen
from Oxygen import Oxygen

class Molecule(ChemicalComponent):

    def __init__(self, name, atoms):
        super().__init__(name)
        for a in atoms:
            if not isinstance(a, Atom):
                raise TypeError("Not an Atom")
        self.__atoms = atoms

    def get_mass(self):
        mass = 0
        for a in self.__atoms:
            mass += a.get_mass()
        return mass

    def get_composition(self):
        composition = {}
        for a in self.__atoms:
            if type(a) in composition:
                composition[type(a)] += 1
            else:
                composition[type(a)] = 1
        return composition

    def __str__(self):
        s = ""
        for type, number in self.get_composition().items():
            s = s+type.get_symbol()
            if number>=2:
                s = s+str(number)
        return s

    def destroy(self):
        atoms = self.__atoms
        self.__atoms = []
        return atoms

    def getAtoms(self):
        return self.__atoms
        
#water = Molecule("Water", [Hydrogen(), Hydrogen(), Oxygen()])
#print(water.get_mass())
#print(water.get_composition())
#print(water)