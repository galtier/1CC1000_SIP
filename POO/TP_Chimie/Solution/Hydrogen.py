from Atom import Atom

class Hydrogen(Atom):

    def __init__(self, name="Hydrogen", neutron_number=0):
        if not 0 <= neutron_number <= 2:
            raise TypeError("Invalid neutron number in Hydrogen")
        super().__init__(name, neutron_number)

    @classmethod
    def get_symbol(cls):
        return 'H'

    @classmethod
    def get_atomic_number(cls):
        return 1

#deuterium = Hydrogen("Deuterium", 1)
#print(deuterium.get_mass())
#print(deuterium)
## sans re-définition de __str__ dans Atom : <__main__.Hydrogen object at 0x7fcb1f4123b0>
## avec re-définition de __str__ dans Atom : [1/2]H
## on aurait pu redéfinir __str__ ici dans Hydrogen mais on aurait dupliqué le code
## dans chacunes des autres classes filles d'Atom :-(