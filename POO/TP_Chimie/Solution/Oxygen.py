from Atom import Atom

class Oxygen(Atom):
    
    def __init__(self, name="Oxygen", neutron_number=8):
        if not 6 <= neutron_number <= 20:
            raise TypeError("Invalid neutron number in Oxygen")
        super().__init__(name, neutron_number)

    @classmethod
    def get_atomic_number(cls):
        return 8

    @classmethod
    def get_symbol(cls):
        return 'O'