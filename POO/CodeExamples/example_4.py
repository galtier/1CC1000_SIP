import math
from abc import ABC, abstractmethod


class Point:
  
  def __init__(self, x, y):
    self.__x = x
    self.__y = y
  
  def get_x(self):
    return self.__x
  
  def get_y(self):
    return self.__y
  
  def __str__(self):
    return "[{}, {}]".format(self.__x, self.__y)


class Shape(ABC):
  _kind = "Shape"
  
  def __init__(self, x, y):
    self.__center = Point(x, y)
  
  def move(self, delta_x, delta_y):
    self.__center = Point(
      self.__center.get_x() + delta_x,
      self.__center.get_y() + delta_y,
    )
   
  @abstractmethod
  def perimeter():
    pass
  
  @classmethod
  def kind(cls):
    return cls._kind
  
  def __str__(self):
    return "center = " + str(self.__center)


class Circle(Shape):
  _kind = "Circle"
  
  def __init__(self, x, y, r):
    super().__init__(x, y)
    self.__radius = r
     
#  def perimeter(self):
#    return 2 * math.pi * self.__radius
  
  def __str__(self):
    return 'Circle: ' + super().__str__() \
       + ', radius = ' + str(self.__radius)

c = Circle(1, 2, 5)
print(str(c)) # Circle: center = [1, 2], radius = 5
c.move(2, -1) # Calling a method of the superclass
print(str(c)) # Circle: center = [3, 1], radius = 5
print(c.kind())     # Circle
