import math


class Point:
  # instance attributes are now created by the constructor
  def __init__(self, x, y):
    self.x = x
    self.y = y
  
  def get_x(self):
    return self.x
  def get_y(self):
    return self.y

'''
class Point:  # using polar coordinates
  def __init__(self, x, y):
    self.radius = math.sqrt(x ** 2 + y ** 2)
    self.azimuth = None
    if self.radius != 0:
        self.azimuth = (1 if y >= 0 else -1) * math.acos(x / self.radius)
  
  def get_x(self):
    return self.radius * math.cos(self.azimuth)
  def get_y(self):
    return self.radius * math.sin(self.azimuth)
'''

#p = Point()          # Error
p = Point(3, 4)      # OK


class Rectangle:
  def __init__(self, top, bottom, left, right):
    self.top_left     = Point(left,  top)
    self.bottom_right = Point(right, bottom)

  def area(self):
    return (self.bottom_right.get_x()
          - self.top_left.get_x())    \
         * (self.bottom_right.get_y()
          - self.top_left.get_y())


r = Rectangle(10, 20, 100, 120)
print(r.area())      # 200

