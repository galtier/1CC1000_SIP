import math


class Point:  # using cartesian coordinates
  def __init__(self, x, y):
    self.__x = x
    self.__y = y
  
  def __get_x(self):
    return self.__x
  def __set_x(self, x):
    self.__x = x
  x = property(__get_x, __set_x)
  
  def __get_y(self):
    return self.__y
  def __set_y(self, y):
    self.__y = y
  y = property(__get_y, __set_y)

  def __str__(self):
    return "[{}, {}]".format(self.x, self.y)
  
'''
class Point:  # using polar coordinates
  def __init__(self, x, y):
    self.__radius = math.sqrt(x ** 2 + y ** 2)
    self.__azimuth = None
    if self.__radius != 0:
        self.__azimuth = (1 if y >= 0 else -1) * math.acos(x / self.__radius)
  
  def __get_x(self):
    return self.__radius * math.cos(self.__azimuth)
  def __set_x(self, x):
    old_y = self.y
    self.__radius = math.sqrt(x ** 2 + old_y ** 2)
    self.__azimuth = None
    if self.__radius != 0:
        self.__azimuth = (1 if old_y >= 0 else -1) * math.acos(x / self.__radius)
  x = property(__get_x, __set_x)

  def __get_y(self):
    return self.__radius * math.sin(self.__azimuth)
  def __set_y(self, y):
    old_x = self.x
    self.__radius = math.sqrt(old_x ** 2 + y ** 2)
    self.__azimuth = None
    if self.__radius != 0:
        self.__azimuth = (1 if y >= 0 else -1) * math.acos(old_x / self.__radius)
  y = property(__get_y, __set_y)

  def __str__(self):
    return "[{}, {}]".format(self.x, self.y)
'''


p = Point(3, 4)
print(str(p))
p.x = p.x + 1
print(str(p))
print(p.__dict__)
